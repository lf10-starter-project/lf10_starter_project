import { Injectable } from '@angular/core';
import {Component} from '@angular/core';
import {Employee} from "./Employee";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {BearerToken} from "./BearerToken";
import {AppComponent} from "./app.component";


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  employeeURL = '/backend';

  constructor(private http: HttpClient) {
  }

  employeeHttpOptions() {
    return {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${localStorage.getItem("bearer")}`)
    };
  }

  employeeBody(employee: Employee) {
    return {"lastName": employee.lastName,
      "firstName": employee.firstName,
      "street": employee.street,
      "postcode": employee.postcode,
      "city": employee.city,
      "phone": employee.phone};
  }

  getEmployees() {
    return this.http.get<Employee[]>(this.employeeURL, this.employeeHttpOptions());
  }

  getEmployeeById(id: number | string) {
    return this.http.get<Employee>(this.employeeURL + '/' + id, this.employeeHttpOptions());
  }

  createEmployee(newEmployee: Employee) {
    return this.http.post<Employee>(this.employeeURL, this.employeeBody(newEmployee), this.employeeHttpOptions());
  }

  updateEmployee(updatedEmployee: Employee) {
    return this.http.put(this.employeeURL + '/' + updatedEmployee.id, this.employeeBody(updatedEmployee), this.employeeHttpOptions());
  }

  deleteEmployee(id: number | string) {
    return this.http.delete<Employee>(this.employeeURL + '/' + id, this.employeeHttpOptions());
  }

  getBearerToken(username: string, password: string) {
    return this.http.post<any>("/bearer", "grant_type=password&client_id=employee-management-service&username="+username+"&password="+password, {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/x-www-form-urlencoded')
      });
  }

}
