import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EmployeesComponent} from "./employees/employees.component";
import {LoginComponent} from "./login/login.component";
import {EmployeeProfileComponent} from "./employee-profile/employee-profile.component";
import {EmployeeCreateComponent} from "./employee-create/employee-create.component";

const paths: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'employees', component: EmployeesComponent},
  {path: 'employee-profile/:id', component: EmployeeProfileComponent},
  {path: 'employee-create', component: EmployeeCreateComponent}
  ];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(paths)
  ],
  exports: [
    RouterModule
  ],
  providers: [

  ]
})
export class AppRoutingModule { }
