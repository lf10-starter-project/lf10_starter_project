export enum EmployeeProfileMode {
  SHOW,
  EDIT,
  CREATE
}
