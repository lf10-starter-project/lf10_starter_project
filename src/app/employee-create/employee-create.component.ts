import { Component, OnInit } from '@angular/core';
import {EmployeeProfileComponent} from "../employee-profile/employee-profile.component";

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
