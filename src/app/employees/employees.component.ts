import { Component, OnInit } from '@angular/core';
import {Employee} from "../Employee";
import {EmployeeService} from "../employee.service";
import {Observable, of} from "rxjs";

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees$: Observable<Employee[]>;

  constructor(private employeeService: EmployeeService) {
    this.employees$ = of([]);
  }

  ngOnInit() {
    this.fetchEmployees();
  }

  fetchEmployees() {
    this.employees$ = this.employeeService.getEmployees();
  }

}
