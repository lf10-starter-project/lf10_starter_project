import {Component, OnInit} from '@angular/core';
import {Employee} from "../Employee";
import {EmployeeService} from "../employee.service";
import {ActivatedRoute, Router} from "@angular/router";
import {EmployeeProfileMode} from "../EmployeeProfileMode";

@Component({
  selector: 'app-employee-profile',
  templateUrl: './employee-profile.component.html',
  styleUrls: ['./employee-profile.component.css']
})
export class EmployeeProfileComponent implements OnInit {

  currentEmployee: Employee | undefined;
  employeeProfileMode: EmployeeProfileMode = EmployeeProfileMode.SHOW;

  constructor(private employeeService: EmployeeService,
              private route: ActivatedRoute,
              private router: Router) {
    this.currentEmployee = new Employee();
  }

  ngOnInit(): void {
    if(this.route.snapshot.url[0].path == "employee-create") {
      this.employeeProfileMode = EmployeeProfileMode.CREATE;
    } else {
      this.fetchCurrentEmployee();
    }
  }

  fetchCurrentEmployee() {
    var id = this.route.snapshot.paramMap.get('id');
    if(id != null) {
      this.employeeService.getEmployeeById(id)
        .subscribe(employee => this.fetchCurrentEmployeeComplete(employee), error => alert("Der Mitarbeiter konnte nicht geladen werden."));
    }
  }

  fetchCurrentEmployeeComplete(employee: Employee) {
    this.currentEmployee = employee;
    this.updateReadonly();
  }

  edit() {
    this.employeeProfileMode = EmployeeProfileMode.EDIT;
    this.updateReadonly();
  }

  abortEdit() {
    if(confirm("Möchten Sie die Änderungen verwerfen?")) {
      this.employeeProfileMode = EmployeeProfileMode.SHOW;
      window.location.reload();
    }
  }

  abortCreate() {
    if(confirm("Möchten Sie die Änderungen verwerfen?")) {
      this.router.navigate(['/employees']);
    }
  }

  deleteEmployee() {
    if(confirm("Möchten Sie diesen Mitarbeiter wirklich unwiederruflich löschen?")) {
      if(this.currentEmployee && this.currentEmployee.id) {
        this.employeeService.deleteEmployee(this.currentEmployee.id)
          .subscribe(() => this.deleteSuccessful(), error => alert("Der Mitarbeiter konnte nicht gelöscht werden."));
      }
    }
  }

  createEmployee() {
    if(confirm("Es wird ein Mitarbeiter mit den angegebenen Daten erstellt.")) {
      var employee = this.getNewEmployee();
      this.employeeService.createEmployee(employee)
        .subscribe(() => this.saveSuccessful(), error => alert("Der Mitarbeiter konnte nicht erstellt werden."));
    }
  }

  updateEmployee() {
    if(confirm("Möchten Sie die Änderungen wirklich übernehmen?")) {
      var employee = this.getNewEmployee();
      this.employeeService.updateEmployee(employee)
        .subscribe(() => this.saveSuccessful(), error => alert("Die Änderungen konnten nicht übernommen werden."));
    }
  }

  saveSuccessful() {
    this.router.navigate(['/employees']);
    alert("Der Mitarbeiter wurde erfolgreich gespeichert.")
  }

  deleteSuccessful() {
    this.router.navigate(['/employees']);
    alert("Der Mitarbeiter wurde erfolgreich gelöscht.")
  }

  getNewEmployee() {
    var firstName = (<HTMLInputElement> document.getElementById("txtfname"))?.value;
    var lastName = (<HTMLInputElement> document.getElementById("txtlname"))?.value;
    var street = (<HTMLInputElement> document.getElementById("txtstreet"))?.value;
    var postcode = (<HTMLInputElement> document.getElementById("txtpostcode"))?.value;
    var city = (<HTMLInputElement> document.getElementById("txtlocation"))?.value;
    var phone = (<HTMLInputElement> document.getElementById("txtphone"))?.value;

    var employee = new Employee();
    employee.id = this.currentEmployee?.id;
    employee.firstName = firstName ? firstName : "";
    employee.lastName = lastName ? lastName : "";
    employee.street = street ? street : "";
    employee.postcode = postcode ? postcode : "";
    employee.city = city ? city : "";
    employee.phone = phone ? phone : "";
    return employee;
  }

  updateReadonly() {
    var readOnly = this.employeeProfileMode == EmployeeProfileMode.SHOW;
    (<HTMLInputElement> document.getElementById("txtfname")).readOnly = readOnly;
    (<HTMLInputElement> document.getElementById("txtlname")).readOnly = readOnly;
    (<HTMLInputElement> document.getElementById("txtstreet")).readOnly = readOnly;
    (<HTMLInputElement> document.getElementById("txtpostcode")).readOnly = readOnly;
    (<HTMLInputElement> document.getElementById("txtlocation")).readOnly = readOnly;
    (<HTMLInputElement> document.getElementById("txtphone")).readOnly = readOnly;
  }
}

