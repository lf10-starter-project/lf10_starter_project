import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {EmployeeService} from "../employee.service";
import {BearerToken} from "../BearerToken";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,
              private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.checkLogin();
  }

  checkLogin() {
    if(this.isLoggedin()) {
      this.loginSuccessful();
    }
  }

  login(username: string, password: string) {
    this.employeeService.getBearerToken(username, password)
      .subscribe(bearerToken => this.loginSuccessful(bearerToken));
  }

  loginSuccessful(bearerToken?: BearerToken) {
    if(bearerToken && bearerToken.access_token) {
      localStorage.setItem("bearer", bearerToken.access_token);
    }
    this.router.navigate(['/employees']);
  }

  isLoggedin(): boolean {
    return false;
  }
}
